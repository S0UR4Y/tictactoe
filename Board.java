public class Board {
    private Square[][] tictactoeBoard;

    public Board() {
        this.tictactoeBoard = new Square[3][3];
        for (int i = 0; i < tictactoeBoard.length; i++) {
            for (int j = 0; j < tictactoeBoard[i].length; j++) {
                this.tictactoeBoard[i][j] = Square.BLANK;
            }
        }
    }

    public boolean placeToken(int row, int col, Square playerToken) {
        if (row > 2 && col > 2 || row < 0 && col < 0) {
            return false;
        }
        if (tictactoeBoard[row][col].equals(Square.BLANK)) {
            tictactoeBoard[row][col] = playerToken;
            return true;
        } else
            return false;
    }

    public boolean checkIfFull() {
        for (int i = 0; i < tictactoeBoard.length; i++) {
            for (int j = 0; j < tictactoeBoard[i].length; j++) {
                if (this.tictactoeBoard[i][j].equals(Square.BLANK)) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkIfWinningHorizontal(Square playerToken) {

        int count = 0;
        for (int i = 0; i < tictactoeBoard.length; i++) {
            count = 0;
            for (int j = 0; j < tictactoeBoard[i].length; j++) {
                if (this.tictactoeBoard[i][j].equals(playerToken)) {
                    count++;
                }
            }

        }
        if (count == 3) {
            return true;
        } else
            return false;
    }

    private boolean checkIfWinningVertical(Square playerToken) {

        int count = 0;
        for (int i = 0; i < tictactoeBoard.length; i++) {
            count = 0;
            for (int j = 0; j < tictactoeBoard[i].length; j++) {
                if (this.tictactoeBoard[j][i].equals(playerToken)) {
                    count++;
                }
            }

        }
        if (count == 3) {
            return true;
        } else
            return false;
    }

    // BONUS QUESTION
    private boolean checkIfWinningDiagonal(Square playerToken) {
        int count = 0;
        for (int i = 0; i < tictactoeBoard.length; i++) {
            if (this.tictactoeBoard[i][i].equals(playerToken)) {
                count++;
            }
        }
        if (count == 3) {
            return true;
        }
        count = 0;
        for (int i = 0; i < tictactoeBoard.length; i++) {
            if (this.tictactoeBoard[i][tictactoeBoard.length - i - 1].equals(playerToken)) {
                count++;
            }
        }
        if (count == 3) {
            return true;
        }
        return false;
    }
    public boolean checkIfWinning(Square playerTocken) {
        if (checkIfWinningVertical(playerTocken) || checkIfWinningHorizontal(playerTocken) || checkIfWinningDiagonal(playerTocken)) 
            return true;
        else
            return false;
    }

    public String toString() {
        String str = "  0 1 2 \n";
        for (int i = 0; i < tictactoeBoard.length; i++) {
            str+=""+i+" ";
            for (int j = 0; j < tictactoeBoard[i].length; j++) {
                str += tictactoeBoard[i][j] + " ";
            }
            str += "\n";
        }
        return str;
    }
}
