
import java.util.Scanner;

public class TicTacToe {
    public static void main(String[] args) {
        System.out.println("welcome");
        Scanner scan = new Scanner(System.in);
        Board game = new Board();
        boolean gameOver = false;
        int player = 1;
        Square playerToken = Square.X;

        int row;
        int col;
        while (!gameOver) {
            System.out.println(game);
            if (player == 1)
                playerToken = Square.X;
            else
                playerToken = Square.O;
            System.out.println("imput row and column");
            row = scan.nextInt();
            col = scan.nextInt();
            while (!game.placeToken(row, col, playerToken)) {
                System.out.println("please enter valid inputs");
                row = scan.nextInt();
                col = scan.nextInt();
            }

            if (game.checkIfWinning(playerToken)) {
                System.out.println(game);
                System.out.println("player " + player + " wins");

                gameOver = true;
            } else if (game.checkIfFull()) {
                System.out.println(game);
                System.out.println("It's a tie");

                gameOver = true;
            } else {
                if (player == 2)
                    player--;
                else
                    player++;

            }

        }
    }
}
